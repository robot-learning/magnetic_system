#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  8 16:26:11 2023

@author: tabor
"""

from std_srvs.srv import SetBool
import shlex
import subprocess
import time
import rospy
import os



def record(file_path,video_setting):
    roslaunch_command = "roslaunch omniSystem video.launch file:={file_name} rate:={rate} config:={video}"
    roslaunch_command = shlex.split(roslaunch_command.format(rate=rate,file_name=file_path,video=video_setting))
    roslaunch_proc = subprocess.Popen(roslaunch_command,stdout=subprocess.PIPE)
    
    
    time.sleep(1)
    
    rospy.wait_for_service('/player/pause_playback')
    rosbag_serv = rospy.ServiceProxy('/player/pause_playback', SetBool)
    
    pause_result = rosbag_serv(True)
    ffmpeg_command = 'ffmpeg -video_size 1680x1050 -framerate {rate} -f x11grab -i :1 "{file_name}_{video_setting}.mp4"'
    ffmpeg_command = shlex.split(ffmpeg_command.format(rate=rate,file_name=file_path[:-4],video_setting=video_setting))
    ffmpeg_proc = subprocess.Popen(ffmpeg_command,stdout=subprocess.PIPE)
    
    pause_result = rosbag_serv(False)
    
    while True:
        output = roslaunch_proc.stdout.readline()
        if output.strip() == b'Done.':
            break
            
    #ffmpeg -video_size 1024x768 -framerate 25 -f x11grab -i $DISPLAY output.mp4
    subprocess.Popen(shlex.split("killall -2 ffmpeg"))
    subprocess.Popen(shlex.split("killall -2 rviz"))
    subprocess.Popen(shlex.split("killall -2 rosmaster"))
    print(file_path)





rate = 50
parent_folder = '/media/tabor/Storage/IJRR_bags_sorted'


for folder in os.listdir(parent_folder)[::-3]:
    folder_path = os.path.join(parent_folder, folder)
    if os.path.isdir(folder_path):
        for file_name in os.listdir(folder_path):
            file_path = os.path.join(folder_path, file_name) 
            if os.path.isfile(file_path) and file_path[-4:] == '.bag':
                print(file_path)

record(file_path,'video')


