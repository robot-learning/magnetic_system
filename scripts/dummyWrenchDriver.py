# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 13:06:56 2020

@author: tabor
"""

import rospy
import numpy as np
from geometry_msgs.msg import WrenchStamped

if __name__ == '__main__':
    pub = rospy.Publisher('ft', WrenchStamped, queue_size=10000)
    rospy.init_node('dummyWrench', anonymous=True)
    
    latest_file = "15hz_3A.csv"
    data = np.loadtxt(open(latest_file, "rb"), delimiter=",", skiprows=3)
    if(len(data.shape)>1):
        startTime = rospy.Time.now()
        bias = np.reshape(np.average(data[:1000,1:],axis=0),(-1,1))
        for i in range(1,data.shape[0]):
            ft = np.reshape(data[i,1:],(-1,1)) -  bias
            dt = (data[i,0] - data[i-1,0])/1000.0
            rospy.sleep(dt)
            wrench = WrenchStamped()
            wrench.header.frame_id = 'world'
            wrench.header.stamp = rospy.Time.from_sec(startTime.to_sec() + (data[i,0] - data[0,0])/1000.0)
            wrench.header.seq = i
            wrench.wrench.force.x = ft[0,0]
            wrench.wrench.force.y = ft[1,0]
            wrench.wrench.force.z = ft[2,0]
            wrench.wrench.torque.x = ft[3,0]
            wrench.wrench.torque.y = ft[4,0]
            wrench.wrench.torque.z = ft[5,0]
            pub.publish(wrench)

    #rospy.spin()