#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 13:05:42 2023

@author: tabor
"""
import sys
import numpy as np
sys.path.append("..")  # Adds higher directory to python modules path.
sys.path.append(
    "../MagneticControlLaw/src"
)  # Adds higher directory to python modules path.


import dataloader

folder = '/home/tabor/IJRR/cylinder'
file_name = '/real_2023-03-03 14:34:58.391420_adaptive_0.015_inverse_'

dataloader.load('/home/tabor/IJRR/sphere_inv/real_2023-02-26 19:47:02.081920_adaptive_0.02_inverse_', globals())
adaptive_parameters = np.array(
    [adaptive.parameters[0]] * 30 + adaptive.parameters
)


import rosbag
bag_file = '/media/tabor/Storage/IJRR_bags_sorted/sphere_inv/2023-02-26-19-47-02.bag'

output_bag_file = bag_file[:-4] + '_updated.bag' 

from matplotlib import cm
from matplotlib.colors import ListedColormap
original_cm = cm.get_cmap('hot_r', 256)
newcolors = original_cm(np.concatenate([np.linspace(0.2,0.85,1000)]))
sigma_color_map = ListedColormap(newcolors)


adaptive_param_index = 2
current_time = -1
with rosbag.Bag(output_bag_file, 'w') as outbag:
    for topic,message,timestamp in rosbag.Bag(bag_file).read_messages():
        if topic == '/visualization_marker' and message.header.frame_id == 'object' and message.ns not in ['current_orientation','object']:
            time = timestamp.to_sec() 
            if time - current_time > 0.5:
                adaptive_param_index +=1
                current_time = time
                max_sigma = 8e7
                min_sigma = 2e6
                
                sigma = adaptive.problem.sigma_multiplier * adaptive_parameters[adaptive_param_index,1,0]
                
                percent_color = (sigma-min_sigma)/(max_sigma-min_sigma)


                colors = sigma_color_map(percent_color)
            
            print(message.ns,message.scale.x/2,adaptive_parameters[adaptive_param_index,0,0])
            

            message.color.r = colors[0]
            message.color.g = colors[1]
            message.color.b = colors[2]
            outbag.write(topic, message, timestamp)

            
            
        else:
            outbag.write(topic, message, timestamp)

        
    