#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 14:22:23 2022

@author: tabor
"""

import matplotlib.pyplot as plt

import numpy as np
from matplotlib.animation import FuncAnimation

import rospy
import tf
from omniSystem.msg import OmniRotationCommand



class Visualiser:
    def __init__(self):
        self.fig, self.ax = plt.subplots()
        self.lines = [plt.plot([], [], 'g'),plt.plot([], [], 'c'),plt.plot([], [], 'b') ]
        self.data = np.zeros((1,3))
        self.times = np.zeros((1))
        
        self.last_length = 0
        
        self.dt = 0.005
    def plot_init(self):
        self.ax.set_xlim(0, 1)
        self.ax.set_ylim(-0.002, 0.002)
        self.ax.plot([0,1e100],[0,0],'k--')
        return self.lines
    


    def callback(self, command):
        
        omniToSensor = transforms[command.magnetNumber] 
        sensorToomni = np.linalg.inv(transforms[command.magnetNumber])
        
        
        d = sensorToomni[:3,3:4]
        dmag = np.linalg.norm(d)
        dUnit = d/ dmag
        mu = 4*np.pi*1e-7
        #print(dUnit)
        scalar = (mu/(4*np.pi*(dmag**3)))
        matrix = 3 * dUnit @ dUnit.T - np.eye(3)
        
        
        
        m_hat =  np.array((command.dipoleAxis.vector.x,command.dipoleAxis.vector.y,command.dipoleAxis.vector.z))
        
        rotation_hat = np.array((command.rotationAxis.vector.x,command.rotationAxis.vector.y,command.rotationAxis.vector.z))
        
        all_but_m = scalar * matrix * command.strength
        steps = int(command.runTime/(1000*self.dt))
        step_angle = 2*np.pi * command.frequency * self.dt
        
        sensor_readings = [np.zeros((3))]
        step_rotation = tf.transformations.rotation_matrix(step_angle,rotation_hat)
        
        start_time = command.header.stamp.secs + command.header.stamp.nsecs * 1e-9
        end_time = start_time + self.dt * steps
        command_times = np.linspace(start_time,end_time,steps)
        times = np.concatenate((np.array([start_time])-1e-7, command_times, np.array([end_time])+1e-7)) 
        #add extra time right before and after command with 0 commanded value
        
        
        for i in range(steps):
            fieldOmni = all_but_m @ m_hat
            fieldSensor = omniToSensor[:3,:3]@fieldOmni
            sensor_readings.append(fieldSensor)
            m_hat = step_rotation[:3,:3] @ m_hat

        sensor_readings.append(fieldSensor*0)

        sensor_readings=np.array(sensor_readings)
        self.data = np.concatenate((self.data,sensor_readings),axis=0)
        self.times = np.concatenate((self.times,times))
    
    def update_plot(self, frame):
        if(len(self.times) != self.last_length):
            self.ax.set_xlim(self.times[-1]-10,self.times[-1])
            for i in range(3):
                self.lines[i][0].set_data(self.times,self.data[:,i])
            self.last_length = len(self.times)
        return self.lines

vis = Visualiser()

rospy.init_node('sensorAnalysis')
rospy.Subscriber("/omniMagnet", OmniRotationCommand, vis.callback)


listener = tf.TransformListener()
rospy.sleep(rospy.Duration(2.0))

transforms = []
for i in range(5):
    pos_quat = listener.lookupTransform('sensor','omni'+str(i),rospy.Time(0))
    transforms.append(tf.transformations.translation_matrix(pos_quat[0]) @ tf.transformations.quaternion_matrix(pos_quat[1]))
# spin() simply keeps python from exiting until this node is stopped


#sensorToMag0 = tf.transformations.translation_matrix((0,0,0.19)) @ tf.transformations.euler_matrix(-np.pi/2, 0,0)


ani = FuncAnimation(vis.fig, vis.update_plot, init_func=vis.plot_init)
plt.show(block= True)
rospy.sleep(rospy.Duration(20.0))

 