#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 12:08:46 2022

@author: tabor
"""
from system import publishCurrentFrame,publishDesiredFrame,publishMarker,publishTFFrame,publishWrench

import numpy as np
from models import construct_omega,rot_z,get_omni_frame
import time
import visualization as visualization
import rospy
from geometry_msgs.msg import WrenchStamped
from visualization_msgs.msg import Marker


class ControllerHelper:
    def __init__(self,env,system,controller,velocityProfile,adaptive,model,solver,useSphere,magnet_remapper = [0,2,3,4] ):
        self.simpleModeledPub = rospy.Publisher('modeledWrenchVis', WrenchStamped, queue_size=10)
        self.trueModeledPub = rospy.Publisher('modeledWrench', WrenchStamped, queue_size=10)
        self.simpleDesiredPub = rospy.Publisher('desiredWrenchVis', WrenchStamped, queue_size=10)
        self.trueDesiredPub = rospy.Publisher('desiredWrench', WrenchStamped, queue_size=10)
        self.adaptive_pub = rospy.Publisher('visualization_marker',Marker,queue_size=10)

        self.model = model
        self.solver = solver
        self.system = system   
        self.env = env
        self.adaptive = adaptive
        self.controller = controller
        self.velocityProfile = velocityProfile
        
        _ =  self.solver.solve_wrench(self.system.state[0][:3,3],np.zeros((6,1)))

        self.useSphere = useSphere        

        self.desired_states = []
        self.controls = []
        self.states = []
        self.pComponents = []
        self.dComponents = []
        self.timestamps = []
    
        self.magnet_remapper = magnet_remapper #skip magnet 1 for 2D experiments
    
    def get_data_dict(self):
        return {
            'desired_states':self.desired_states,
            'controls':self.controls,
            'states':self.states
                }


    def convertCommand(self,control,state):
    
        magnetNum =control.mag_num
    
        if(self.useSphere):
            strength = control.dipole_strength
            RotAxis = construct_omega(np.array([control.psi,control.xi])).reshape((3))
        else:
            strength = control.dipole_strength
            position = state[0][:3,3]
            chosenMagnetPose = (np.array(self.model.magnets) - np.transpose(position)).tolist()[magnetNum]
            omniFrame = get_omni_frame(chosenMagnetPose)
            
            
            if(control.model_num == 2):
                R = np.dot(omniFrame,rot_z(control.theta))
                originalRotAxis =  np.array([1,0,0]).reshape((3,1))
            if(control.model_num==0):
                originalRotAxis =  np.array([0,0,1]).reshape((3,1))
                R = omniFrame
            if(control.model_num==1):
                originalRotAxis =  np.array([0,0,-1]).reshape((3,1))
                R = omniFrame
    
            RotAxis = np.dot(R,originalRotAxis)[:,0]
        #find start of rotation, any vector perpendicular to RotAxis
        genericZ = np.array([0,0,1])
        dipoleAxis = np.cross(RotAxis,genericZ)
        dipoleAxis = dipoleAxis / np.linalg.norm(dipoleAxis)
        #print('orig magnet command',magnetNum)
        remapped_magnet_num = self.magnet_remapper[magnetNum]
        #print('mod magnet command',magnetNum)
    
        return remapped_magnet_num,strength,RotAxis,dipoleAxis
    
    def step(self,experiment_time=0,i=0,wallRiding = False, stabilizing=False, runAdaptive=False):
        timeSince = rospy.Time.now().to_sec() - self.system.lastTime
        start_time = rospy.Time.now().to_sec() 
        global desiredState
        if(timeSince > 0.5):
            print('OLD STATE DATA ' +str(timeSince))
            return
            
        
        state = self.system.getState()
        
        if(stabilizing):
            desiredState = self.env.start_state
        elif wallRiding:
            desiredState = np.zeros((12,1))
        else:
            desiredState = self.velocityProfile.get_desired_state(i,state)
    
          
        desired_wrench,p,d = self.controller.get_desired_wrench(state,desiredState)
       # print(i,'after error',desiredState[5])
        
        if(wallRiding):
            desired_wrench = desired_wrench*0
            legTime = 180
            if(experiment_time < legTime):
                desired_wrench[0] = 1
            elif(experiment_time < legTime*2):
                desired_wrench[1] = 1
            elif(experiment_time < legTime*3):
                desired_wrench[0] = -1
            elif(experiment_time < legTime*4):
                desired_wrench[1] = -1
            elif(experiment_time < legTime*5):
                desired_wrench[1] = 1
            elif(experiment_time < legTime*6):
                desired_wrench[0] = 1
            elif(experiment_time < legTime*7):
                desired_wrench[1] = -1
            elif(experiment_time < legTime*8):
                desired_wrench[0] = -1
        publishWrench(desired_wrench,self.simpleDesiredPub,self.trueDesiredPub)
        control =  self.solver.solve_wrench(state[0][:3,3],desired_wrench)
        
        
        
        expectedWrench = self.model.forward_pass(state[0][:3,3],control)
        publishWrench(expectedWrench,self.simpleModeledPub,self.trueModeledPub)
        magnetNum,strength,RotAxis,dipoleAxis = self.convertCommand(control,state)
        self.system.command(magnetNum,strength,RotAxis,dipoleAxis)
        
        control_time = rospy.Time.now().to_sec() - start_time
        if runAdaptive:
            self.adaptive.step(state, control)
            adaptive_time = rospy.Time.now().to_sec() - start_time - control_time

            print(self.solver.model.object_properties.radius,self.solver.model.object_properties.sigma)
            publishMarker(self.solver.model.object_properties, -1,0.7, self.adaptive_pub)
            
            if i % 480 == 240:
                publishMarker(self.solver.model.object_properties,i, 0.35, self.adaptive_pub, 'adaptive_8m')
            elif i % 240 == 120:
                publishMarker(self.solver.model.object_properties,i, 0.35, self.adaptive_pub, 'adaptive_4m')
            elif i % 120 == 60:
                publishMarker(self.solver.model.object_properties,i, 0.35, self.adaptive_pub, 'adaptive_2m')
            elif i % 60 == 30:
                publishMarker(self.solver.model.object_properties,i, 0.35, self.adaptive_pub, 'adaptive_60s')
            elif i % 30 == 15:
                publishMarker(self.solver.model.object_properties,i, 0.35, self.adaptive_pub, 'adaptive_30s')
    
    
    
        
        publishDesiredFrame(self.system,np.copy(desiredState))
        publishCurrentFrame(self.system,state)
        #print(i,'after pub',desiredState[5])
    
        #no point logging if not running true trajectory
        #if(stabilizing or wallRiding):
        #    return
        
        self.pComponents.append(p)
        self.dComponents.append(d)
        self.controls.append(control)
        
        
        #state = self.system.getState()
        self.states.append((np.copy(state[0]),np.copy(state[1]),np.copy(state[2])))
        self.desired_states.append(desiredState)
        self.timestamps.append(rospy.Time.now().to_sec())
        
        
        
        expectedUnit = expectedWrench[:2,0]/np.linalg.norm(expectedWrench[:2])
        desiredUnit = desired_wrench[:2,0]/np.linalg.norm(desired_wrench[:2])
        
        angle = np.arccos(np.dot(expectedUnit,desiredUnit))
    
        if runAdaptive:
            total_time = rospy.Time.now().to_sec() - start_time
        
            wasted_time = total_time - control_time - adaptive_time
            if(total_time > 1):
                
                print('times',control_time,adaptive_time,wasted_time)
        # if i % 100 == 1:
        #     adaptive.plot([0.02,5.8e7,1])
            #     try:
        #         system.plot()
        #         pass
        #     except:
        #         pass
        #print(angle,   time.time()-s)
        # if(angle > 0.5):
        #     print(control)
        #     print(desired_wrench)
        #     print(expectedWrench)
    
       # print(i,desiredState[5])
        return magnetNum,strength,RotAxis,dipoleAxis
        
    def runVisualizationCode(self,numSteps):
        visStart = time.time()
        positions = np.array([state[0][:3,3] for state in self.states[-numSteps:]])
        linearVelocity = np.array([np.dot(state[0][:3,:3],state[2][:,0]) for state in self.states[-numSteps:]])
        rotationalVelocity = np.array([np.dot(state[0][:3,:3],state[1][:,0]) for state in self.states[-numSteps:]])
        transforms = np.array([state[0] for state in self.states[-numSteps:]])
        #desiredFinalTF = buildTF(desiredState[:3,0],desiredState[3:6,0])
        remainingError = np.array([self.controller.compute_error(state[0],self.states[-numSteps][0]) for state in self.states[-numSteps:]])
          
        
        #visualization stuff
        self.env.radius*=0.5
        anim = visualization.animate_2d_path(self.env,positions,transforms,self.controls,name="",save_plot_steps=self.velocityProfile.switching_times,save=False)
        npdesired_states = np.array(self.desired_states)
        
        Plots = {'Position (m)':np.transpose(positions),
                 'Velocity (m/s)':np.transpose(linearVelocity),
                 'Rot velocity (_/s)':np.transpose(rotationalVelocity),
                 'Desired position (m)':np.transpose(npdesired_states[-numSteps:,:3,0]),
                 'Desired velocity (m/s) ':np.transpose(npdesired_states[-numSteps:,6:9,0])}
        
        error = np.sum((np.transpose(npdesired_states[-numSteps:,:3,0]) - np.transpose(positions))**2)
        print(error)
        
        visualization.plot_stuff(Plots,'Controller')
        
        Plots = {'Position Error(m)':np.abs(np.transpose(remainingError[-numSteps:,:3,0])),
                 'Orientation Error(radians)':-np.transpose(remainingError[-numSteps:,3:6,0])}
        
        visualization.plot_stuff(Plots,'Error')
        
        
        Plots = {'Desired oritentation (m)':np.transpose(npdesired_states[-numSteps:,3:6,0]),
                 'Desired orientation velocity (m/s) ':np.transpose(npdesired_states[-numSteps:,9:12,0])}
        
        
        visualization.plot_stuff(Plots,'Controller')
        
        
        
        
        return anim