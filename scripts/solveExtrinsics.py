#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 01:55:47 2022

@author: tabor
"""
import rospy
import numpy as np
from apriltag_ros.msg import AprilTagDetectionArray
import tf
from ll4ma_opt.problems import Problem
import pytorch3d.transforms
import torch

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import mpl_toolkits.mplot3d.axes3d as p3
import sys

sys.path.append(
    "../MagneticControlLaw/src"
)  # Adds higher directory to python modules path.

from visualization import draw_origin

rospy.init_node('camera_extrinsics', anonymous=True)




positions = [np.zeros((4))]*4
z_value = 0.15 - 0.097 # mag to center - mag to top of mag
magnets = np.array([(0.1, 0.1,z_value,1) 
                    ,(-0.1, 0.1,z_value,1)
                    ,(-0.1, -0.1, z_value,1)
                    ,(0.1, -0.1, z_value,1)])

id_mapping = [0,1,2,3]
def callback(msg):
    for detection in msg.detections:
        
        quat = [    detection.pose.pose.pose.orientation.x,     detection.pose.pose.pose.orientation.y,
                detection.pose.pose.pose.orientation.z,     detection.pose.pose.pose.orientation.w]
        euler = tf.transformations.euler_from_quaternion( quat)
        
        theta = euler[2]
        
        positions[id_mapping[detection.id[0]-10]] = np.array([detection.pose.pose.pose.position.x,
                                                  detection.pose.pose.pose.position.y,
                                                  detection.pose.pose.pose.position.z,1])
rospy.Subscriber("/tag_detections", AprilTagDetectionArray, callback,queue_size=10)




class Extrin_problem(Problem):
    def __init__(self):
        super().__init__()
        self.initial_solution = np.ones((6,1)) * 0.0001
        self.initial_solution[2] = 0.5
        self.min_bounds[3] = -np.pi
        self.min_bounds[4] = -np.pi
        self.min_bounds[5] = -np.pi
        
        self.max_bounds[3] = np.pi
        self.max_bounds[4] = np.pi
        self.max_bounds[5] = np.pi

        self.measured_positions = [torch.tensor(position,dtype=torch.float64) for position in positions]
        self.true_position = [torch.tensor(magnet,dtype=torch.float64) for magnet in magnets]
    def tensor_cost(self, x):
        T = self.get_T(x)
        cost = torch.tensor([0.0],dtype=torch.float64)
        for i in range(4):
            transformed_position = T @ self.true_position[i]
            error = self.measured_positions[i] - transformed_position
            cost = cost + error.T @ error
        return cost
    
    def get_T(self,x):
        angles = x[3:,0]
        position = x[:3].reshape((3,1))
        R = pytorch3d.transforms.euler_angles_to_matrix(angles,'ZYX')
        T = torch.cat((torch.cat((R,position),1),torch.tensor(((0.,0,0,1))).reshape((1,4))),0)
        return T

    def size(self):
        return 6
    
    def visualize_optimization(self, iterates):
        fig = plt.figure()
        ax = p3.Axes3D(fig)
        ax.scatter(magnets[:,0],magnets[:,1],magnets[:,2],c='red')
        draw_origin(ax)
        
        T = self.get_T(torch.tensor(iterates[-1]))
        
        positions = []
        for i in range(4):
            transformed_position = torch.linalg.inv(T) @ self.measured_positions[i]
            positions.append(transformed_position.numpy())
        
        np_positions = np.array(positions)
        ax.scatter(np_positions[:,0],np_positions[:,1],np_positions[:,2],c='blue')
            

rospy.sleep(rospy.Duration(3))
from ll4ma_opt.solvers.line_search import NewtonMethod,GradientDescent
optimizerParams = {'alpha':1.0,'rho':0.9,'c1':1e-3,'FP_PRECISION': 1e-8,'min_alpha': 1e-10}



while not rospy.is_shutdown():
    prob =Extrin_problem()
    optimizer = NewtonMethod(problem=prob,**optimizerParams)
    result = optimizer.optimize(prob.initial_solution,max_iterations=50000)
    solution = result.solution
    print(prob.get_T(torch.tensor(solution)).numpy())
    print(result.cost)
print(solution[0,0],solution[1,0],solution[2,0],solution[3,0],solution[4,0],solution[5,0])
prob.visualize_optimization(result.iterates)