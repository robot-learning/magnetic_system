# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 13:39:42 2020

@author: tabor
"""
try:
    a=First
except:    
    from system import *
    system = omniSystem()
    First = False
    rospy.sleep(30)

systemXs = np.array(system.x)
systemYs = np.array(system.y)
systemTs = np.array(system.t)
systemThetas = np.array(system.theta)


x = []
xdot = []
y = []
ydot = []
theta = []
thetadot = []
ts = []
slinear= system.linearSmoothingTerm
srotation = system.rotationalSmoothingTerm
for i in range(50,systemXs.shape[0]):
    x_spline = UnivariateSpline(systemTs[:i], systemXs[:i],s=slinear)
    xdot_spline = x_spline.derivative()
    
    y_spline = UnivariateSpline(systemTs[:i], systemYs[:i],s=slinear)
    ydot_spline = y_spline.derivative()
    
    theta_spline = UnivariateSpline(systemTs[:i], systemThetas[:i],s=srotation)
    thetadot_spline = theta_spline.derivative()
    
    x.append(x_spline(systemTs[i-1]))
    xdot.append(xdot_spline(systemTs[i-1]))
    y.append(y_spline(systemTs[i-1]))
    ydot.append(ydot_spline(systemTs[i-1]))
    theta.append(theta_spline(systemTs[i-1]))
    thetadot.append(thetadot_spline(systemTs[i-1]))
    ts.append(systemTs[i])

nameList = ['x','y','theta']

fig,axs = plt.subplots(len(nameList))
fig.suptitle('only past information Smoothing')

dataList = [(x,xdot,systemXs),
            (y,ydot,systemYs),
            (theta,thetadot,systemThetas)]
for index in range(len(nameList)):

    axs[index].set_title(nameList[index])
    axs[index].plot(systemTs,dataList[index][2], 'ro', lw=3)

    axs[index].plot(ts,dataList[index][0], 'g', lw=3)
    axs[index].plot(ts,100*np.array(dataList[index][1]), 'b', lw=3)       
    
    
fig,axs = plt.subplots(len(nameList))
fig.suptitle('Full Smoothing')

dataList = [(x_spline(ts),xdot_spline(ts),systemXs),
            (y_spline(ts),ydot_spline(ts),systemYs),
            (theta_spline(ts),thetadot_spline(ts),systemThetas)]
for index in range(len(nameList)):

    axs[index].set_title(nameList[index])
    axs[index].plot(systemTs,dataList[index][2], 'ro', lw=3)

    axs[index].plot(ts,dataList[index][0], 'g', lw=3)
    axs[index].plot(ts,100*np.array(dataList[index][1]), 'b', lw=3)    

fig,axs = plt.subplots(3)
fig.suptitle('Path')
axs[0].set_title('Full Smoothing')
axs[0].plot(x_spline(ts),y_spline(ts))

axs[1].set_title('un smoothed')
axs[1].plot(systemXs,systemYs)

axs[2].set_title('only past information Smoothing')
axs[2].plot(x,y)