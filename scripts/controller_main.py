#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 17:40:28 2023

@author: tabor
"""

from system import omniSystem, SystemMode
import subprocess, shlex
from datetime import datetime
import numpy as np
import rospy
import sys

sys.path.append("..")  # Adds higher directory to python modules path.
sys.path.append(
    "../MagneticControlLaw/src"
)  # Adds higher directory to python modules path.

from tqdm import tqdm
import numpy as np
from PDController import PDController
from discreteTimeSimulator import DiscreteTimeSimulator, simulate_experiment
from environments import (
    PlanarProblem,
    SpaceProblem,
    PlanarEnvironments,
    SpaceEnvironments,
)
from se3Utils import build_tf
import matplotlib.pyplot as plt
import time
import visualization
import DummyProfile
import SplineProfile
import TrapProfile
from Adaptive import Adaptive

from controller_helper import ControllerHelper

from models import MultiMagModel
from solvers.modelOptimization import MultiMagSolver
from solvers.axisAlignedSolver import AxisAlignedSolver
from solvers.devinSolver import DevinSolver
from solvers.devinSolver2 import DevinSolver2

from ObjectProperties import (
    ObjectProperties,
    compute_boat_properties,
    compute_sphere_properties,
)
from sysIDoptimization import RadiusSysid, SeperateRadiusSysid, MassSysID

import threading


class KeyboardThread(threading.Thread):
    def __init__(self, name="keyboard-input-thread"):
        super(KeyboardThread, self).__init__(name=name)
        self.start()

    def run(self):
        input()
        global stabilized
        stabilized = True


def stabilize():
    global helper
    kthread = KeyboardThread()
    global stabilized
    stabilized = False
    rate = rospy.Rate(1.0)  # 1hz
    while not (stabilized or rospy.is_shutdown()):
        response = helper.step(stabilizing=True)
        if response:
            magnetNum,strength,RotAxis,dipoleAxis = response
            if(strength < 5 and False):
                print('Strength low, sufficiently stabilized')
                stabilized = True
        rate.sleep()
    print("Finished stabilizing object")


if __name__ == "__main__":

    mode = SystemMode.CONTROL

    use_special_case_solver = False
    use_sphere = True

    run_adaptive = False and use_sphere
    inverse_dynamics = False and use_sphere and not use_special_case_solver
    controller_knows_boat = True

    env = SpaceProblem(SpaceEnvironments.CUBE)

    env.trajectory_dimensions = 'xyz'
    
    #env.times = (np.array(env.times) * 3).astype(np.int).tolist()
    #env.times[0] = int(env.times[0]*1.5)

    adaptive_model = RadiusSysid

    sigma_copper = 5.87e07
    sigma_al = 3.77e07

    # controller object
    initial_radius_guess = 0.02
    initial_sigma_guess = sigma_al
    sim_time = np.sum(env.times)

    
    mass_initial, moi_initial = compute_sphere_properties(8940, initial_radius_guess)
    object_properties_approximate = ObjectProperties(
        mass=mass_initial,
        MOI=moi_initial,
        radius=initial_radius_guess,
        sigma=initial_sigma_guess,
    )

    # boat object
    boat_mass, boat_moi = compute_boat_properties(0.059, 0.075, 0.07, 0.031)
    boat_properties = ObjectProperties(mass=boat_mass, MOI=boat_moi)
    
    #boat_properties = ObjectProperties(mass = 0.83,MOI= np.diag((0,0,1.13e-3)))

    #boat_properties = ObjectProperties(mass=0.5, MOI=np.diag((0, 0, 0)))

    drag_terms =  (8.12e-3, 2.15, 1.13e-5, 1.90e-4)
    
    if controller_knows_boat:
        controller_boat = boat_properties
    else:
        controller_boat = None

    solver_model = MultiMagModel(
        env.magnets, object_properties_approximate, use_sphere
    )  # approximate object properties for controller

    if use_special_case_solver:
        solver = AxisAlignedSolver(solver_model, env.trajectory_dimensions)
        solver = DevinSolver(solver_model)
    else:
        solver = MultiMagSolver(
            model=solver_model,
            state_cost_string=env.trajectory_dimensions,
            object_properties=object_properties_approximate,
            sphere_model=use_sphere,
            inverse_dynamics=inverse_dynamics,
            boat=controller_boat,
            num_processes=8,
        )

    if inverse_dynamics:
        controller = PDController(env.start_state, 0.01, 0.004, 0.01, 0.004, 0, 0)
    elif use_special_case_solver:
        controller = PDController(env.start_state, 0.0005, 0.0, 0.1, 0.0, 0.0, 0.000)
    else:
        controller = PDController(
            env.start_state, 0.005, 0.000001, 0.005, 0.000001, 0.0, 0.000
        )

    velocity_profile = SplineProfile.SplineProfile(env, env.times)
    adaptive = Adaptive(
        env.magnets,
        "xyt",
        object_properties_approximate,
        25,
        adaptive_model,
        controller_boat,
        drag_terms=drag_terms
    )
    #adaptive.problem.regularization_multiplier = 0
    record_command = 'rosbag record -a -x "/camera/image_color|/diagnostics|/camera/image_raw|/camera/image_mono|/camera/image|/camera/image_rect|/camera/image_rect_fast|/image_view/.*|/aruco_single/.*|/original_camera/.*|/tag_detections_.*"'
    record_command = shlex.split(record_command)
    kill_command = shlex.split("killall -2 record")

    system = omniSystem()
    rospy.sleep(rospy.Duration(2.0))
    print("starting")
    rate = rospy.Rate(1.0)  # 1hz

    global helper
    helper = ControllerHelper(
        env=env,
        system=system,
        controller=controller,
        velocityProfile=velocity_profile,
        adaptive=adaptive,
        model=solver_model,
        solver=solver,
        useSphere=use_sphere,
        magnet_remapper = [0,3]
    )

    if mode == SystemMode.CONTROL:
        print("stabilizing")
        stabilize()
        # env.startState = system.getStateVector()
        if run_adaptive:
            #pass
            object_properties_approximate.radius = 0.01
            object_properties_approximate.sigma = sigma_copper
            adaptive.reset_current_solution(object_properties_approximate)
            # object_properties_approximate.sigma = 11.6e7
        print(solver.model.cylinder_model.object_properties)
        print("start rosbag")
        startTime = datetime.now()
        rosbag_proc = subprocess.Popen(record_command)
        system.reset_path()
        try:
            for i in tqdm(range(sim_time)):
                if rospy.is_shutdown():
                    print('ending')
                    break
                elapsed_time = (datetime.now()-startTime).total_seconds()
                helper.step(elapsed_time, i, wallRiding=mode==SystemMode.WALL_RIDING, runAdaptive=run_adaptive)
                rate.sleep()
        except:
            solver.pool.close()
            #del solver
            
        anim = helper.runVisualizationCode(i)
        rosbag_proc.send_signal(subprocess.signal.SIGINT)
        kill_proc = subprocess.Popen(kill_command)

        from dataloader import save
        del adaptive.optimizer #is dumb
        
        
        save("../MagneticControlLaw/data/real_" + str(startTime) + '_' +
             ('adaptive_' + str(initial_radius_guess) + '_' if run_adaptive else '') +
             ('inverse_' if inverse_dynamics else solver.__str__().split(' ')[0].split('.')[-1])
             , [globals(),helper.get_data_dict()])
    if mode == SystemMode.WALL_RIDING:
        for i in tqdm(range(180*8)):
            helper.step(0, i, wallRiding=True)
            print('')
            rate.sleep()
    if mode == SystemMode.DEBUG:
        for j in range(3):
            for mag in [0,3]:
                debugTime = 10
                freq = 1

                print("mag " + str(mag))
                print("x")
                system.command(
                    mag,
                    40,
                    [1, 0, 0],
                    [0, 1, 0],
                    runTime=int(debugTime * 1000),
                    freq=freq,
                )
                rospy.sleep(rospy.Duration(debugTime))
                print("y")
                system.command(
                    mag,
                    40,
                    [0, 1, 0],
                    [1, 0, 0],
                    runTime=int(debugTime * 1000),
                    freq=freq,
                )
                rospy.sleep(rospy.Duration(debugTime))
                print("z")
                system.command(
                    mag,
                    40,
                    [0, 0, 1],
                    [0, 1, 0],
                    runTime=int(debugTime * 1000),
                    freq=freq,
                )
                rospy.sleep(rospy.Duration(debugTime))

        print("finished")
