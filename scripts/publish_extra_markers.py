#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 17:13:07 2022

@author: tabor
"""

import rospy
from visualization_msgs.msg import Marker
from copy import deepcopy

import numpy as np

rospy.init_node('switch_markers', anonymous=True)

vis_pub = rospy.Publisher('visualization_marker',Marker,queue_size=10)


def callback(msg):
    if msg.ns[-3:] != '_aluminum'[-3:]:
        marker = deepcopy(msg)
        marker.ns = msg.ns + '_aluminum'
        marker.color.r = 173/256
        marker.color.g = 178/256
        marker.color.b = 189/256
        vis_pub.publish(marker)

rospy.Subscriber("visualization_marker", Marker, callback)

rospy.spin()

# marker = Marker()
# marker.ns = 'new'
# marker.header.stamp = rospy.Time.now() 
# marker.header.frame_id = 'center'
# marker.type = 1
# marker.action = 0

# marker.color.r = 0.722
# marker.color.g = 0.451
# marker.color.b = 0.2

# marker.color.a = 0.8
# marker.pose.position.y = - 0.14 
# marker.pose.position.z = - 0.05 

# marker.pose.orientation.x = np.sqrt(2)/2
# marker.pose.orientation.w = np.sqrt(2)/2

# marker.scale.x = 0.0254
# marker.scale.y = 0.0254
# marker.scale.z = 0.052

# vis_pub.publish(marker)

