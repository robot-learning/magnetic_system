#!/usr/bin/env python

# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 17:04:39 2020

@author: tabor
"""

import rospy
import time
import numpy as np
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
import sys
sys.path.append("..") # Adds higher directory to python modules path.
from MagneticControlLaw.exponentialSimulator import exponentialSimulator, sphereProperties
from geometry_msgs.msg import WrenchStamped

def simUpdate(wrench):
    ft[0,0] = wrench.wrench.force.x
    ft[1,0] = wrench.wrench.force.y
    ft[2,0] = wrench.wrench.force.z
    ft[3,0] = wrench.wrench.torque.x
    ft[4,0] = wrench.wrench.torque.y
    ft[5,0] = wrench.wrench.torque.z
    
    global simulator
    state = simulator.simulate(ft)
    pose = PoseStamped()
    pose.header.stamp=wrench.header.stamp 
    pose.header.frame_id = 'world'
    pose.pose.position.x = state[0]
    pose.pose.position.y = state[1]
    pose.pose.position.z = state[2]
    #TODO do euler to quaternion math
    pose.pose.orientation.w = 1
    pub.publish(pose)
    print(pose)

    return state

if __name__ == '__main__':
    pub = rospy.Publisher('obj_pose', PoseStamped, queue_size=10)
    rospy.init_node('sim', anonymous=True)
    rospy.Subscriber("ft", WrenchStamped, simUpdate)

    startState = np.zeros((12,1))
    ft = np.zeros((6,1))
    dt = 0.001
    global simulator
    simulator = exponentialSimulator(startState,dt,sphereProperties(8940,0.05)) #density, diamter
    rospy.spin()
