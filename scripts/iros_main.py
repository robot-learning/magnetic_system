#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 13:26:39 2022

@author: tabor
"""

from system import *
import subprocess, shlex
from datetime import datetime
import numpy as np
import rospy
import time
import sys
sys.path.append("..") # Adds higher directory to python modules path.
sys.path.append("../MagneticControlLaw") # Adds higher directory to python modules path.

from SplineProfile import SplineProfile


from se3Utils import buildTF

from environments import PlanarProblem


if __name__ == '__main__':
    
    env = PlanarProblem(3)
    system = omniSystem()    
    dt = 4
    
    
    command = 'rosbag record -a -x "/camera/image_color|/diagnostics|/camera/image_raw|/camera/image_mono|/camera/image|/camera/image_rect|/camera/image_rect_fast|/image_view/.*|/aruco_single/.*|/original_camera/.*"'
    command = shlex.split(command)
    killCommand = shlex.split('killall -2 record')
    rosbag_proc = subprocess.Popen(command)

 
    rospy.sleep(rospy.Duration(0.25))
    print('starting')
 
    
    freq = 15
    strength = 40
    selected_magnets = [0,2]
    direction = 1
    dipole_rotations = [[0,0,-1* direction],[0,0,1*direction]]
    dipole_start = [0,1,0]

    total_time = 2000    
    time_steps = int(total_time/dt)
    for t in range(time_steps):
        if(rospy.is_shutdown()):
            break
        for mag,dipole_rotation in zip(selected_magnets,dipole_rotations):
            if(rospy.is_shutdown()):
                break
            mag_time = dt/len(selected_magnets)
            
            system.command(mag,strength,dipole_rotation,dipole_start,runTime=int(mag_time*1000),freq=freq)
            rospy.sleep(rospy.Duration(mag_time))
            
    rosbag_proc.send_signal(subprocess.signal.SIGINT)
    kill_proc = subprocess.Popen(killCommand)
