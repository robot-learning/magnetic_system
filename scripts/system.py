# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 13:11:14 2020

@author: tabor
"""

import rospy
import numpy as np
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import WrenchStamped
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import TransformStamped, PointStamped
from apriltag_ros.msg import AprilTagDetectionArray
from visualization_msgs.msg import Marker

from std_msgs.msg import String
from omniSystem.msg import OmniRotationCommand
import tf
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline

from enum import Enum
from matplotlib import cm
from matplotlib.colors import ListedColormap

class SystemMode(Enum):
    CONTROL = 1
    DEBUG = 2
    WALL_RIDING = 3
    CONSOLE = 4

class omniSystem:
    def __init__(self):
        rospy.init_node('poseToState', anonymous=True)
        self.state = (np.eye(4),np.zeros((3,1)),np.zeros((3,1)))
        
        self.keepStates = 1000
        self.x = []
        self.y = []
        self.z = []
        self.theta = []
        self.t = []
        
        self.thetaLoops = 0 
        self.lastAbsTheta = 0
        self.lastTime = 0
        self.linearSmoothingTerm = None#1e-6 #0.05
        self.rotationalSmoothingTerm = 0.5
        rospy.Subscriber("/simple_aruco_detector/transforms", TransformStamped, self.transform_subscriber,queue_size=10)
        rospy.Subscriber("/tag_detections", AprilTagDetectionArray, self.april_tag_subscriber,queue_size=10)
        rospy.Subscriber("/underwater_position", PointStamped, self.position_point_subscriber,queue_size=10)

        
        self.pub = rospy.Publisher('omniMagnet', OmniRotationCommand, queue_size=10)

        self.reset_pub = rospy.Publisher('syscommand', String, queue_size=10)

        self.listener = tf.TransformListener()
        self.br = tf.TransformBroadcaster()

        rospy.sleep(rospy.Duration(2))        
        print('made listener')
        
        #fig,axs = plt.subplots(3)
        #fig.suptitle('Smoothed Data')
        #self.fig = fig
        #self.axs = axs


#        publishTFFrame(self)
    def reset_path(self):
        self.reset_pub.publish('reset')

    def getStateVector(self):
        self.state = self.getState()
        rot = np.array(tf.transformations.euler_from_matrix(self.state[0])).reshape((3,1))
        position = self.state[0][:3,3:4]
        linearVel = np.dot(self.state[0][:3,:3],self.state[2])
        rotVel = np.dot(self.state[0][:3,:3],self.state[1])
        stateVector = np.concatenate((position, rot,linearVel ,rotVel))
        return stateVector



    def command(self,magnetNum,strength,RotAxis,dipoleAxis,runTime= 1000,freq=15.0):
        if(np.isnan(strength)):
            return
        test = OmniRotationCommand()
        test.header.stamp=rospy.Time.now() 
        test.header.frame_id = 'center'
        test.strength = strength
        test.magnetNumber = magnetNum
        
        dAxis = Vector3Stamped()
        dAxis.vector.x = dipoleAxis[0]
        dAxis.vector.y = dipoleAxis[1]
        dAxis.vector.z = dipoleAxis[2]
        dAxis.header.frame_id = 'center'
        dAxis.header.stamp = rospy.Time(0)
        test.dipoleAxis=self.listener.transformVector3("omni"+str(magnetNum),dAxis)

        rAxis = Vector3Stamped()
        rAxis.vector.x = RotAxis[0]
        rAxis.vector.y = RotAxis[1]
        rAxis.vector.z = RotAxis[2]
        rAxis.header.frame_id = 'center'
        rAxis.header.stamp = rospy.Time(0)
        test.rotationAxis=self.listener.transformVector3("omni"+str(magnetNum),rAxis)
        test.runTime = runTime - 2 #just to account for overhead in system remove 2 ms
        test.frequency = freq
        rospy.logdebug("sent command to magnet %d, with power %f",magnetNum,strength)
        self.pub.publish(test)
        
    def transform_subscriber(self,transform):
         if(transform.child_frame_id != 'marker_id24'):
             return
         quat = [transform.transform.rotation.x, transform.transform.rotation.y,
                 transform.transform.rotation.z, transform.transform.rotation.w]

         euler = tf.transformations.euler_from_quaternion( quat)
         
         theta = euler[2]
         x = transform.transform.translation.x
         y = transform.transform.translation.y
         time = transform.header.stamp.to_sec()
         self.stateUpdate(time,x,y,0,theta)
       
    def april_tag_subscriber(self,msg):
        for detection in msg.detections:
            if detection.id[0] != 0:
                continue               
            
            pose = PoseStamped()
            pose.header.frame_id = 'camera'
            pose.pose = detection.pose.pose.pose
            
            transformed_pose = self.listener.transformPose('center', pose)
            quat = [    transformed_pose.pose.orientation.x,     transformed_pose.pose.orientation.y,
                    transformed_pose.pose.orientation.z,     transformed_pose.pose.orientation.w]
            euler = tf.transformations.euler_from_quaternion( quat)
            theta = euler[2]
            x = transformed_pose.pose.position.x
            y = transformed_pose.pose.position.y
            time = msg.header.stamp.to_sec()
            self.stateUpdate(time,x,y,0,theta)
        
    def position_point_subscriber(self,msg):
        
        self.listener.waitForTransform("center", "tracking", msg.header.stamp, rospy.Duration(0.1))

        #msg.header.frame_id = 'center'
        
        transformed_pose = self.listener.transformPoint('center', msg)
        x = transformed_pose.point.x
        y = transformed_pose.point.y
        z = transformed_pose.point.z
        time = msg.header.stamp.to_sec()

        self.stateUpdate(time,x,y,z,0)
    
    def stateUpdate(self,time,x,y,z,absolute_theta):
        start = rospy.Time.now().to_sec()

        self.x.append(x)
        self.y.append(y)
        self.z.append(z)

        self.t.append(time)        
        
        if abs(absolute_theta-self.lastAbsTheta) > 5:
            self.thetaLoops -= np.sign(absolute_theta)
        self.theta.append(absolute_theta + 2*np.pi*self.thetaLoops)
        self.lastAbsTheta = absolute_theta
        
        #self.state = self.getState()
        self.lastTime = time
        end = rospy.Time.now().to_sec()
        if(end - time > 0.1):
            print('off by ',end - time)
            print('processing took ',end-start)
            print('images ', len(self.x))
        #print(self.t)
        #publishTFFrame(self)
    def fitSplines(self):
        splines = []
        x_spline = UnivariateSpline(self.t[-self.keepStates:], self.x[-self.keepStates:],s=self.linearSmoothingTerm)
        xdot_spline = x_spline.derivative()
        
        y_spline = UnivariateSpline(self.t[-self.keepStates:], self.y[-self.keepStates:],s=self.linearSmoothingTerm)
        ydot_spline = y_spline.derivative()
        
        z_spline = UnivariateSpline(self.t[-self.keepStates:], self.z[-self.keepStates:],s=self.linearSmoothingTerm)
        zdot_spline = z_spline.derivative()
        
        theta_spline = UnivariateSpline(self.t[-self.keepStates:], self.theta[-self.keepStates:],s=self.rotationalSmoothingTerm)
        thetadot_spline = theta_spline.derivative()
        
        splines.append([x_spline,xdot_spline])
        splines.append([y_spline,ydot_spline])
        splines.append([z_spline,zdot_spline])
        splines.append([theta_spline,thetadot_spline])

        return splines
    def plot(self):
        print("'don't use")
        splines = self.fitSplines()
        nameList = ['x','y','z','theta']

        axs = self.axs
        xs = np.linspace(np.min(self.t[-self.keepStates:]),np.max(self.t[-self.keepStates:]), 100)
        dataList = [self.x,self.y,self.z,self.theta]
        
        for index in range(len(nameList)):
            
            axs[index].set_title(nameList[index])
            axs[index].plot(self.t, dataList[index], 'ro', ms=5)
    
            axs[index].plot(xs, splines[index][0](xs), 'g', lw=3)
            axs[index].plot(xs, splines[index][1](xs)*100, 'b', lw=3)        
            plt.pause(0.0001)

        self.fig.canvas.draw()
    
    def getState(self):
        splines = self.fitSplines()
        t = np.max(self.t)
        x = self.x[-1]#splines[0][0](t)
        xdot = splines[0][1](t)
        
        y = self.y[-1]#splines[1][0](t)
        ydot = splines[1][1](t)
        
        z = self.z[-1]#splines[2[0](t)
        zdot = splines[2][1](t)
        
        
        theta = self.theta[-1]#splines[3][0](t)
        thetadot = splines[3][1](t)
        
        position = [x, y, z]
        transform = tf.transformations.euler_matrix(0, 0, theta)
        transform[:3,-1] = np.array(position)
        
        
        worldRotVel = np.array([0,0,thetadot]).reshape((3,1))
        Ωsc = (transform[:3,:3].T @ worldRotVel)[:3]
        Vsc = (transform[:3,:3].T @ np.array([xdot,ydot,zdot]).reshape((3,1)))[:3]

        return (transform,Ωsc,Vsc)

def flipTheta(thetas):
    thetas = np.array(thetas)
    new = thetas - np.sign(thetas) * np.pi 
    return new
def publishDesiredFrame(system,desiredState):
    rot = tf.transformations.quaternion_from_euler(*desiredState[3:6])
    system.br.sendTransform(desiredState[:3],rot,rospy.Time.now(),'goal','center')
    
def publishCurrentFrame(system,state):
    system.br.sendTransform(state[0][:3,3],[0,0,0,1],rospy.Time.now(),'current','center') 
def publishTFFrame(system,model):
    for i in range(4):
        frameI = i +1
        (trans,rot) = system.listener.lookupTransform('center','omni'+str(frameI),rospy.Time(0))
        frame = model.getFrame(model.getOffsetMagnets(system.state)[i])
        #print(frame)
        stupidHack = np.zeros((4,4))
        stupidHack[:3,:3] = frame
        stupidHack[3,3] = 1
        #print(stupidHack)
        newRot = tf.transformations.quaternion_from_matrix(stupidHack)
        #print(newRot)
        frame = 'control'+str(frameI)
        #print(frame)
        system.br.sendTransform(trans,newRot,rospy.Time.now(),frame,'center')


original_cm = cm.get_cmap('hot_r', 256)
newcolors = original_cm(np.concatenate([np.linspace(0.2,0.85,1000)]))

sigma_color_map = ListedColormap(newcolors)


def publishMarker(object_properties, count, alpha, marker_publisher, ns = 'live_adaptive'):
    marker = Marker()
    marker.ns = ns
    marker.header.stamp = rospy.Time.now() 
    marker.header.frame_id = 'object'
    marker.type = 2
    marker.action = 0
    
    max_sigma = 8e7
    min_sigma = 2e6
    
    percent_color = (object_properties.sigma-min_sigma)/(max_sigma-min_sigma)
    
    
    colors = sigma_color_map(percent_color)
    
    
    marker.color.r = colors[0]
    marker.color.g = colors[1]
    marker.color.b = colors[2]
    
    marker.color.a = alpha
    
    marker.pose.orientation.z = 1
    
    marker.scale.x = 2 * object_properties.radius
    marker.scale.y = 2 * object_properties.radius
    marker.scale.z = 2 * object_properties.radius
    
    marker.text = str(object_properties.sigma)
    marker.id = count
    marker_publisher.publish(marker)
    

def publishWrench(w,simpleWrenchPublisher,trueWrenchPublisher):
    wrench = WrenchStamped()
    wrench.header.stamp=rospy.Time.now() 
    wrench.header.frame_id = 'current'
    mag = 1
    oldMag = np.sqrt(np.sum(np.sum(np.square(w[:3]))))
    if(oldMag < 1e-20):
        oldMag = 1e-20
    #print(oldMag)
    unitW = w[:3]/oldMag
    newW = mag*unitW
    wrench.wrench.force.x = newW[0]
    wrench.wrench.force.y = newW[1]
    wrench.wrench.force.z = newW[2]
    wrench.wrench.torque.x = 0
    wrench.wrench.torque.y = 0
    wrench.wrench.torque.z = 0
    simpleWrenchPublisher.publish(wrench)
    
    wrench.wrench.force.x = w[0]
    wrench.wrench.force.y = w[1]
    wrench.wrench.force.z = w[2]
    wrench.wrench.torque.x = w[3]
    wrench.wrench.torque.y = w[4]
    wrench.wrench.torque.z = w[5]
    trueWrenchPublisher.publish(wrench)