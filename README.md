magnetic_system
controller_main.py is the main file


To use main machine runs
```roslaunch omniSystem system_control.launch ```
for control experiments 
and 
```roslaunch omniSystem open_loop_system.launch ```
for open loop experiments

hardware is run by sshing into partner PC and running
```roslaunch omniSystem hardware.launch ```

Then run python scripts controller_main.py or iros_main.py to run experiment. Inside those python files is experiment specific settings. Both also automatically run rosbag to record experiments. Additionally the control experiments instruct the object to travel to the start position and stabilize there, when the object is at desired start pose and perfectly steady enter a character and hit enter into python terminal to trigger it beginning experiment.






To clear trajectories from history, from any terminal run 
```rostopic pub /syscommand std_msgs/String "data: 'reset'"``` 

To clear old sphere objects from path go to marker on rviz -> namespaces and uncheck and recheck the objects you want to remove. They are not continuously republished and only exist inside the buffer within rviz